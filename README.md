# DevOPS Loïc

**Bases de la mise en réseau Networking 101**



### Création d'un réseau VPC personnalisé
La première étape consiste à créer un réseau VPC personnalisé. Ce réseau sera le fondement de notre infrastructure cloud, permettant la communication entre les différentes ressources que nous allons déployer.

```bash
gcloud compute networks create taw-custom-network --subnet-mode custom
```

### Ajout de sous-réseaux
Une fois le réseau VPC en place, on procède à la création de sous-réseaux dans différentes régions. Cela nous permet de segmenter notre réseau en blocs plus petits, facilitant la gestion et l'application des politiques de sécurité.

```bash
gcloud compute networks subnets create subnet-europe-west1 \
   --network taw-custom-network \
   --region europe-west1 \
   --range 10.0.0.0/16

gcloud compute networks subnets create subnet-us-west1 \
   --network taw-custom-network \
   --region us-west1 \
   --range 10.1.0.0/16

gcloud compute networks subnets create subnet-us-east1 \
   --network taw-custom-network \
   --region us-east1 \
   --range 10.2.0.0/16
```

### Configuration des règles de pare-feu
Pour sécuriser notre réseau et contrôler l'accès aux instances VM, on définit des règles de pare-feu. Ici, on crée une règle permettant le trafic HTTP entrant.

```bash
gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http
```

### Déploiement d'instances VM
Enfin, on déploie des instances VM dans notre réseau, en les plaçant dans les sous-réseaux appropriés. Ces instances serviront d'hôtes pour nos applications et services.

```bash
gcloud compute instances create us-test-01 \
--subnet subnet-europe-west1 \
--zone europe-west1-b \
--machine-type e2-standard-2 \
--tags ssh,http,rules
```


### Ajout de règles de pare-feu supplémentaires
Pour une couverture de sécurité plus complète, il est important d'ajouter des règles de pare-feu supplémentaires qui permettent le trafic ICMP (pour le ping), SSH (pour la connexion sécurisée à nos instances), et potentiellement RDP (pour l'accès à distance sous Windows).

```bash
# Autoriser ICMP (ping)
gcloud compute firewall-rules create nw101-allow-icmp \
--allow icmp --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags icmp

# Autoriser SSH
gcloud compute firewall-rules create nw101-allow-ssh \
--allow tcp:22 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags ssh

# Autoriser RDP (facultatif pour les instances Windows)
gcloud compute firewall-rules create nw101-allow-rdp \
--allow tcp:3389 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags rdp
```

### Création d'instances VM supplémentaires
Pour tester la connectivité et la performance entre différentes zones et régions, il est judicieux de déployer des instances VM supplémentaires dans les sous-réseaux que nous avons créés.

```bash
# Créer une instance VM dans us-west1
gcloud compute instances create us-test-02 \
--subnet subnet-us-west1 \
--zone us-west1-b \
--machine-type e2-standard-2 \
--tags ssh,http

# Créer une instance VM dans us-east1
gcloud compute instances create us-test-03 \
--subnet subnet-us-east1 \
--zone us-east1-c \
--machine-type e2-standard-2 \
--tags ssh,http
```

### Tester la connectivité et la performance


```bash
# Test ping 
ping -c 3 <us-test-02-external-ip-address>
ping -c 3 <us-test-02-external-ip-address>
ping -c 3 <us-test-03-external-ip-address>
ping -c 3 us-test-02.ZONE
```





**Principes de base de Terraform**


### Création du fichier de configuration Terraform

```bash
touch instance.tf
```
Cette commande crée un fichier vide nommé `instance.tf`. Dans Terraform, les fichiers `.tf` contiennent la définition de l'infrastructure à déployer. Ici, `instance.tf` servira à définir une instance VM sur Google Cloud.

### Structure du fichier `instance.tf`

Le contenu ajouté à `instance.tf` définit une ressource de type `google_compute_instance`, qui est une instance VM dans Google Cloud. Voici une explication de chaque élément de la configuration :


### Initialisation de Terraform

```bash
terraform init
```
Cette commande initialise le répertoire de travail Terraform, téléchargeant et installant les plugins nécessaires, dont le fournisseur Google Cloud (`google`). C'est la première étape à exécuter avant de planifier ou d'appliquer votre configuration.

### Planification des modifications

```bash
terraform plan
```
`terraform plan` permet de visualiser les modifications que Terraform s'apprête à effectuer sur l'infrastructure, sans les appliquer. C'est utile pour vérifier que la configuration produit l'effet désiré.

### Application de la configuration

```bash
terraform apply
```
Exécute le plan d'action généré par `terraform plan`, créant ou modifiant l'infrastructure réelle sur Google Cloud conformément à la définition dans `instance.tf`. Terraform demande une confirmation avant de procéder; répondre `yes` appliquera les changements.

### Vérification de l'état de l'infrastructure

```bash
terraform show
```
Après l'application de la configuration, `terraform show` permet d'inspecter l'état actuel de l'infrastructure gérée par Terraform. Cela inclut les détails de toutes les ressources créées, comme les instances VM.


Lien de mon docker Hub : https://hub.docker.com/r/redwolf77/testloic
